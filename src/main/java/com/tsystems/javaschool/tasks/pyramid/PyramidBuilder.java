package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throw {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.isEmpty()) {
            throw new CannotBuildPyramidException();
        }
        int rowsAmount = getRowsAmount(inputNumbers.size());
        Collections.sort(inputNumbers);
        return fillPyramid(inputNumbers, rowsAmount);
    }

    /**
     * Build 2-d array and fill in values.
     *
     * @param inputNumbers source list.
     * @param rowsAmount number of rows.
     * @return 2-d array.
     */
    private int[][] fillPyramid(List<Integer> inputNumbers, int rowsAmount){
        int columnAmount = rowsAmount * 2 - 1;
        int[][] pyramid = new int[rowsAmount][columnAmount];
        int firstNumberPosition = columnAmount / 2;
        int currentListElement = 0;
        for (int i = 0; i < rowsAmount; i++) {
            int countOfIterations = 0;
            int elementPosition = firstNumberPosition;
            while (countOfIterations <= i) {
                pyramid[i][elementPosition] = inputNumbers.get(currentListElement);
                elementPosition = elementPosition + 2;
                currentListElement++;
                countOfIterations++;
            }
            firstNumberPosition--;
        }
        return pyramid;
    }

    /**
     * Get number of rows.
     *
     * @param inputListSize size of source list.
     * @return number of rows.
     * @throw CannotBuildPyramidException if triangle can not be built.
     */
    private int getRowsAmount(int inputListSize) {
        int rowsAmount = 0;
        while (inputListSize > 0) {
            inputListSize = inputListSize - (++rowsAmount);
        }
        if (inputListSize != 0) {
            throw new CannotBuildPyramidException();
        }
        return rowsAmount;
    }

}

