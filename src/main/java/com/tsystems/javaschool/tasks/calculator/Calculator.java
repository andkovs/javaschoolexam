package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        return round(getResult(getPostfixExpression(statement)), 4);
    }

    /**
     * Get not rounded string result.
     *
     * @param postfixExpressionStack stack with postfix expression.
     * @return string result.
     */
    private String getResult(Stack<String> postfixExpressionStack) {
        if (postfixExpressionStack == null) {
            return null;
        }
        Stack<String> resultStack = new Stack<String>();
        while (!postfixExpressionStack.isEmpty()) {
            if (isDigit((String) postfixExpressionStack.peek())) {
                resultStack.push(postfixExpressionStack.pop());
            } else {
                String operand2 = resultStack.pop();
                String operand1 = resultStack.pop();
                String operator = postfixExpressionStack.pop();
                resultStack.push(calculate(operand1, operand2, operator));
            }
        }
        if (resultStack.size() > 1) {
            return null;
        }
        return resultStack.pop();

    }

    /**
     * Build stack with postfix expression.
     *
     * @param exp input statement.
     * @return stack with postfix expression.
     */
    private Stack getPostfixExpression(String exp) {
        if (exp == null) {
            return null;
        }
        if (!checkExpression(exp)) {
            return null;
        }
        String[] symbolsArray = splitExpression(exp);
        Stack<String> result = new Stack<String>();
        Stack<String> operator = new Stack<String>();

        for (int i = 0; i < symbolsArray.length; i++) {
            if (isDigit(symbolsArray[i])) {
                result.push(symbolsArray[i]);

            } else {
                if (!symbolsArray[i].equals(")")) {
                    if (!symbolsArray[i].equals("(") && operator.size() > 0 && !hasPriority(symbolsArray[i], operator.peek())) {
                        result.push(operator.pop());
                        operator.push(symbolsArray[i]);
                    } else {
                        operator.push(symbolsArray[i]);
                    }
                } else {
                    boolean findBrace = false;
                    while (!operator.isEmpty()) {
                        if ((operator.peek()).equals("(")) {
                            operator.pop();
                            findBrace = true;
                            break;
                        }
                        result.push(operator.pop());
                    }
                    if (!findBrace) {
                        return null;
                    }
                }
            }
        }
        for (int i = 0; i <= operator.size(); i++) {
            if (operator.peek().equals("(") || operator.peek().equals(")")) {
                return null;
            }
            result.push(operator.pop());
        }
        return reverseStack(result);
    }

    /**
     * Check operators priority.
     *
     * @param current current operator.
     * @param pop     operator from stack.
     * @return true, if current's priority higher.
     */
    private boolean hasPriority(String current, String pop) {
        if ((current.equals("*") || current.equals("/")) && (pop.equals("+") || pop.equals("-"))) {
            return true;
        }
        if (pop.equals("(") || pop.equals(")")) {
            return true;
        }
        return false;
    }

    /**
     * Check digit.
     *
     * @param value string math's unit.
     * @return true, if value is digit.
     */
    private boolean isDigit(String value) {
        if (value.matches("[0-9.]*")) {
            return true;
        }
        return false;
    }

    /**
     * Split input statement.
     *
     * @param exp string statement.
     * @return array with math's units.
     */
    private String[] splitExpression(String exp) {
        return exp.split("(?<=[-+*/()])|(?=[-+*/()])");
    }

    /**
     * Reverse stack.
     *
     * @param stack input stack.
     * @return stack after reverse.
     */
    private Stack reverseStack(Stack stack) {
        Stack resultStack = new Stack();
        while (!stack.isEmpty()) {
            resultStack.push(stack.pop());
        }
        return resultStack;
    }

    /**
     * Calculate result.
     *
     * @param operand1 string first operand.
     * @param operand2 string second operand.
     * @param operator string operator.
     * @return result string.
     */
    private String calculate(String operand1, String operand2, String operator) {
        Double o1 = Double.parseDouble(operand1);
        Double o2 = Double.parseDouble(operand2);
        Double resultDouble = 0.0;
        if (operator.equals("+")) {
            resultDouble = o1 + o2;
        }
        if (operator.equals("-")) {
            resultDouble = o1 - o2;
        }
        if (operator.equals("*")) {
            resultDouble = o1 * o2;
        }
        if (operator.equals("/")) {
            if (o2 == 0.0) {
                return null;
            }
            resultDouble = o1 / o2;
        }
        return resultDouble.toString();
    }

    /**
     * Validate input statement.
     *
     * @param exp string statement.
     * @return true, if statement is valid.
     */
    private boolean checkExpression(String exp) {
        Pattern pattern = Pattern.compile(new String("[0-9()+\\-*\\/.]+"));
        Matcher matcher = pattern.matcher(exp);
        if (!matcher.matches()) {
            return false;
        }
        pattern = Pattern.compile(new String("[\\+\\-\\*\\/\\.]{2,}"));
        matcher = pattern.matcher(exp);
        if (matcher.find()) {
            return false;
        }
        return true;
    }

    /**
     * Round result.
     *
     * @param value  string result.
     * @param places amount of significant digits.
     * @return result rounded string.
     */
    private String round(String value, int places) {
        if (value == null) {
            return null;
        }
        double result = Double.parseDouble(value);
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(result);
        bd.setScale(places, RoundingMode.UP);
        String resultString;
        if (result % 1 == 0) {
            Integer resultInteger = (int) result;
            resultString = resultInteger.toString();
        } else {
            resultString = new Double(result).toString();
        }
        return resultString;
    }

}
